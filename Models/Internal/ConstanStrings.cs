﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.Models.Internal
{
    public class ConstanStrings
    {
        public const string THGameId = "57bca793-2632-4f0e-a7a8-031b3b83f6dd";
        public const string ETHGameId = "081bd80a-a53e-4002-8793-c89957db71b8";



        //Sample Exchange Rate
        public const int GoldToSilver = 10;
        public const int SilverToCopper = 10;
        public const int GoldToCopper = 100;
        public const int GoldToDubloons = 1000;
        public const int SilverToDubloons = 100;
        public const int CopperToDubloons = 10;
    }
}