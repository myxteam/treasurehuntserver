﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.Models.Internal
{
    public class Game
    {
            public int Id { get; set; }
            public System.Guid GameID { get; set; }
            public string Name { get; set; }
        
    }
}