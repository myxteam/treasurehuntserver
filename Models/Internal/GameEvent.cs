﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.Models.Internal
{
    public class GameEvent
    {
            public int Id { get; set; }
            public int Amount { get; set; }
            public DateTime Date { get; set; }
            public int CurrencyId { get; set; }

    }
}