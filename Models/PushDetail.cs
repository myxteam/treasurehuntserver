﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.Models
{
    public class PushDetail
    {        
        public string[] Users { get; set; }
        public Dictionary<string, string> Properties { get; set; }
    }
}