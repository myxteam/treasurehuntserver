﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.Models
{
    public class NotificationHubRegistration
    {
        public string Platform { get; set; }
        public string UserName { get; set; }
        public bool NotifyOnRegister { get; set; }                        
        public string DeviceId { get; set; }        
    }
}