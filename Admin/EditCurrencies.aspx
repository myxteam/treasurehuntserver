﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/adminMaster.Master" AutoEventWireup="true" CodeBehind="EditCurrencies.aspx.cs" Inherits="TreasureGameWeb.Admin.EditCurrencies" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Edit Currencies</h1>
    <h4>Max 15 Currencies</h4>
    <p>
        <table style="width: 100%;">
            <tr>
                <td style="width: 88px">
                    <asp:Label ID="Label5" runat="server" Text="New:"></asp:Label>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="TB_NEWCUR" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:Button ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" Text="Add" />
                </td>
                <td style="width: 116px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 88px">
                    &nbsp;</td>
                <td style="width: 87px">
                    &nbsp;</td>
                <td style="width: 116px">
                    &nbsp;</td>
                <td style="width: 116px">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:Label ID="Name" runat="server" Text="Name"></asp:Label>
                </td>
                <td style="width: 87px">
                    <asp:Label ID="Label1" runat="server" Text="Buy"></asp:Label>
                </td>
                <td style="width: 116px">
                    <asp:Label ID="Label2" runat="server" Text="Sell"></asp:Label>
                </td>
                <td style="width: 116px">
                    <asp:Label ID="Label4" runat="server" Text="Order"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Action"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_1"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_1" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_1" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                         <asp:TextBox ID="corder_1" runat="server"></asp:TextBox></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_2"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_2" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_2" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                           <asp:TextBox ID="corder_2" runat="server"></asp:TextBox></td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_3"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_3" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_3" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
         <asp:TextBox ID="corder_3" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_3" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_4"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_4" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_4" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                  <asp:TextBox ID="corder_4" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_4" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_5"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_5" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_5" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                   <asp:TextBox ID="corder_5" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_5" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_6"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_6" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_6" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
               <asp:TextBox ID="corder_6" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_6" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

                       <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_7"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_7" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_7" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
               <asp:TextBox ID="corder_7" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_7" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_8"   ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_8" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_8" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
            <asp:TextBox ID="corder_8" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_8" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_9"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_9" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_9" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                  <asp:TextBox ID="corder_9" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_9" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>


            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_10"   ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_10" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_10" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                   <asp:TextBox ID="corder_10" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_10" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_11"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_11" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_11" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                          <asp:TextBox ID="corder_11" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_11" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_12"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_12" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_12" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="corder_12" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_12" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_13"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_13" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_13" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="corder_13" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_13" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_14"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_14" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_14" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                 <asp:TextBox ID="corder_14" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_14" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>

            <tr>
                <td style="width: 88px">
                    <asp:TextBox ID="cname_15"  ReadOnly="true" runat="server"></asp:TextBox>
                </td>
                <td style="width: 87px">
                    <asp:TextBox ID="cbuy_15" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                    <asp:TextBox ID="csell_15" runat="server"></asp:TextBox>
                </td>
                <td style="width: 116px">
                 <asp:TextBox ID="corder_15" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Button ID="Button_15" runat="server" Text="Delete" OnClick="Button14_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 88px">&nbsp;</td>
                <td style="width: 87px">&nbsp;</td>
                <td style="width: 116px">&nbsp;</td>
                <td style="width: 116px">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 88px">
                    <asp:Button ID="Button16" runat="server" OnClick="Button16_Click" Text="Save" />
                </td>
                <td style="width: 87px">&nbsp;</td>
                <td style="width: 116px">&nbsp;</td>
                <td style="width: 116px">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </p>

</asp:Content>
