﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TreasureGameWeb.DAL;

namespace TreasureGameWeb.Admin
{
    public partial class EditCurrencies : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            base.OnPreRenderComplete(e);

            //Check Later
            //Model1Container TZE = new Model1Container();


            //var cu = TZE.Currencies.Where(t => t.Active == true);
            //int i = 0;
            //foreach (var x in cu)
            //{
            //    i++;
            //    if (i > 15)
            //    {
            //        return;
            //    }

            //    string cx_name = x.Name;
            //    string cx_buy = x.Buy.ToString();
            //    string cx_sell = x.Sell.ToString();
            //    //string cx_order = x.order.ToString();


            //    ((TextBox)this.Form.FindControl("MainContent").FindControl("cname_" + i.ToString())).Text = cx_name;
            //    ((TextBox)this.Form.FindControl("MainContent").FindControl("cbuy_" + i.ToString())).Text = cx_buy;
            //    ((TextBox)this.Form.FindControl("MainContent").FindControl("csell_" + i.ToString())).Text = cx_sell;
            //    //((TextBox)this.Form.FindControl("MainContent").FindControl("corder_" + i.ToString())).Text = cx_order;

            //}
        }

        protected void Button14_Click(object sender, EventArgs e)
        {
            String btnId = ((Button)sender).ID;

            int iid = int.Parse(btnId.Substring(btnId.IndexOf("_")+1));
            if (iid == 1 || iid == 2)
            {
                return;
            }

            Model1Container TZE = new Model1Container();
            string cx_name = ((TextBox)this.Form.FindControl("MainContent").FindControl("cname_" + iid)).Text;

            var x = TZE.Currencies.Where(t => t.Name == cx_name).FirstOrDefault();

            x.Active = false;

            TZE.SaveChanges();

            Response.Redirect("EditCurrencies.aspx");
        
        }


        //save
        protected void Button16_Click(object sender, EventArgs e)
        {
            Model1Container TZE = new Model1Container();
            for (int i = 1; i < 16; i++)
            {

                string cx_name = ((TextBox)this.Form.FindControl("MainContent").FindControl("cname_" + i.ToString())).Text;
                string cx_buy = ((TextBox)this.Form.FindControl("MainContent").FindControl("cbuy_" + i.ToString())).Text;
                string cx_sell = ((TextBox)this.Form.FindControl("MainContent").FindControl("csell_" + i.ToString())).Text;
                string cx_order = ((TextBox)this.Form.FindControl("MainContent").FindControl("corder_" + i.ToString())).Text;

                double cz_buy, cz_sell = -1;
                int cz_order = -1;

                if (double.TryParse(cx_buy, out cz_buy) && double.TryParse(cx_sell, out cz_sell) && int.TryParse(cx_order, out cz_order))
                {
                    if (cz_buy > 0 && cz_sell > 0 && cz_order > 0)
                    {
                        var x = TZE.Currencies.Where(t => t.Name == cx_name).FirstOrDefault();
                        //x.order = cz_order;
                        //x.Buy = cz_buy;
                        //x.Sell = cz_sell;
                    }
                }
            }
            TZE.SaveChanges();
            Response.Redirect("EditCurrencies.aspx");
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Model1Container TZE = new Model1Container();
            string x1 = TB_NEWCUR.Text.Trim();

            var y = TZE.Currencies.Where(t => t.Active == true).Count();
            if (y > 14) return;
            var x = TZE.Currencies.Where(t => t.Name == x1).FirstOrDefault();
            if(x == null)
            {
                Currency cu = new Currency();
                //cu.Buy = 1;
                //cu.Sell = 1;
                cu.Name = x1;
                //cu.order = 1000;
                cu.Active = true;
                TZE.Currencies.Add(cu);
                TZE.SaveChanges();
                Response.Redirect("EditCurrencies.aspx");
            }
        }
    }
}