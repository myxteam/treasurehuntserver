﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using TreasureGameWeb.DAL;

namespace TreasureGameWeb.Admin
{
    public partial class EditUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Search_Click(object sender, EventArgs e)
        {
            Model1Container TZE = new Model1Container();
            User user = null;
            String uname = this.SearchTB.Text.Trim();

            if (!String.IsNullOrEmpty(uname))
            {
                user = TZE.Users.Where(t => t.Username == uname).FirstOrDefault();
            }

            if (user != null)
            {
                this.userlabel.Text = user.Username;
                var x = user.Balances.ToList();
                var y = TZE.Currencies.Where(t => t.Active == true).OrderBy(t=>t.Id);

                int lk = 1;
                foreach(var o in y)
                {
                    ((Label)this.Form.FindControl("MainContent").FindControl("clabel_" + lk.ToString())).Text = o.Name;
                    var z = x.Where(t => t.Id == o.Id).FirstOrDefault();
                    if(z == null)
                    {
                        ((TextBox)this.Form.FindControl("MainContent").FindControl("cTextBox_" + lk.ToString())).Text = "0";
                    }
                    else
                    {
                        ((TextBox)this.Form.FindControl("MainContent").FindControl("cTextBox_" + lk.ToString())).Text = z.Amount.ToString();
                    }
                    lk++;
                }
            }
        }

                //for (int i = 0; i < 15; i++)
                //{
                //    if (x.Count < i + 1)
                //    {
                //        return;
                //    }
                //    if (x[i] != null)
                //    {
                //        int l = x[i].Id;
                //        switch (i + 1)
                //        {
                //            case 1:
                //                {
                //                    this.clabel_1.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_1.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 2:
                //                {
                //                    this.clabel_2.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_2.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 3:
                //                {
                //                    this.clabel_3.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_3.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 4:
                //                {
                //                    this.clabel_4.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_4.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 5:
                //                {
                //                    this.clabel_5.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_5.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 6:
                //                {
                //                    this.clabel_6.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_6.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 7:
                //                {
                //                    this.clabel_7.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_7.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 8:
                //                {
                //                    this.clabel_8.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_8.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 9:
                //                {
                //                    this.clabel_9.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_9.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 10:
                //                {
                //                    this.clabel_10.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_10.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 11:
                //                {
                //                    this.clabel_11.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_11.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 12:
                //                {
                //                    this.clabel_12.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_12.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 13:
                //                {
                //                    this.clabel_13.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_13.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 14:
                //                {
                //                    this.clabel_14.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_14.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //            case 15:
                //                {
                //                    this.clabel_15.Text = TZE.Currencies.Where(t => t.Id == l).FirstOrDefault().Name;
                //                    this.cTextBox_15.Text = x[i].Amount.ToString();
                //                    break;
                //                }
                //        }
                //    }
                //}

        //    }
        //}

        protected User getUser()
        {
            return null;
        }

        protected ICollection<DAL.Balance> UserCurrencies()
        {
            return null;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string user1 = this.userlabel.Text;
            Model1Container TZE = new Model1Container();
            User user = null;
            String uname = user1;

            if (!String.IsNullOrEmpty(uname))
            {
                user = TZE.Users.Where(t => t.Username == uname).FirstOrDefault();
            }

            if (user == null)
            {
                return;
            }

            var x = user.Balances;
            #region loop
            for (int i = 0; i < 15; i++)
            {
                switch (i + 1)
                {
                    case 1:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_1.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_1.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 2:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_2.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_2.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 3:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_3.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_3.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 4:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_4.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_4.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 5:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_5.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_5.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 6:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_6.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_6.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 7:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_7.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_7.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 8:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_8.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_8.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 9:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_9.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_9.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 10:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_10.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_10.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 11:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_11.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_11.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 12:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_11.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_11.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 13:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_13.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_13.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 14:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_14.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_14.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                    case 15:
                        {
                            int amm = -1;
                            int.TryParse(this.cTextBox_15.Text.Trim(), out amm);

                            var y = TZE.Currencies.Where(t => t.Name == this.clabel_15.Text).FirstOrDefault();
                            if (y != null && amm >= 0)
                            {
                                int fk = y.Id;
                                var z = x.Where(t => t.Id == fk).FirstOrDefault();
                                if (z == null)
                                {
                                    Balance pfe = new Balance();
                                    pfe.Amount = amm;
                                    pfe.Id = fk;
                                    x.Add(pfe);
                                }
                                else
                                {
                                    z.Amount = amm;
                                }

                            }
                            break;
                        }
                }
            }
            #endregion
            TZE.SaveChanges();
            Response.Redirect("EditUser.aspx");
        }
    }
}
