﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.Class
{
    public class ConstantStringsMessages
    {
        public const string ERROR_OCCURED = "An Error occured";
        public const string INVALID_USERNAME = "Invalid Username/Password";
        public const string INVALID_USER = "Invalid User";
        public const string USERNAME_ALREADY_TAKEN = "The Username is already taken";
        public const string INVALID_ID = "Invalid Id";
        public const string VALIDATION_ERROR = "Validation Error";
        public const string SERVER_NAME = "Treasure Hunt Server";
        public const string SERVER_NAME_CREEL = "Treasure Hunt Server Creel";
        public const string APPLICATION_NAME = "Treasure Hunt Application";
        public const string CREEL_ERROR_MESSAGE = "There is Something wrong with creel. please try again later";
        public const string CREEL_INVALID_ID = "Invalid Id Please Register At Creel";
        public const string CREEL_EXISTING_REQUEST = "There Is Already A Pending Request for this Device that need Your Approval";
        public const string CREEL_APP_APPROVAL = "This game is not yet approved. Please allow the game to use your Creel.";
        public const string CREEL_BUSINESS_RULES = "Creel Invalid Transfer";
        public const string CREEL_PENDING_AUTHORIZATION = "You are not yet Authorize. Please try again after you have approved our authorization request";
        public const string CREEL_DENIED_AUTHORIZATION = " Authorization of The app has been denied. Would you like to try authorizing this app again? ";
        public const string CREEL_PENDING_DEAUTHORIZE = "You Have a Pending Application that need to be Deauthorize to Continue. Would you Deauthorize?";
        public const string CREEL_INVALID_FUND_TRANSFER = "Insufficient Funds to be Transferred";
        public const string CREEL_INVALID_FUND_BUY = "Cannot Buy or Transfer due Insufficient Funds";
        public const string CREEL_UNLINK_FINISH = "Unlink Creel Finish";
        public const string CREEL_RETURN_EMPTY_GUID = "Creel Returned Empty GUID";
        public const string CREEL_CODE_ACCEPTING_INSTRUCTION = "Gold Sent ";
        public const string WALLET_NO_CURRENCY_FOUND = "No currencies found.";
        public const string NO_USER_FOUND = "No User found.";

        #region Creel code Error
        public const int CREEL_APPROVAL_CODE = 10100;
        public const int CREEL_VALIDATION_CODE = 2000;
        public const int CREEL_INVALID_ID_CODE = 1100;
        public const int CREEL_INSUFFICIENT_FUNDS_CODE = 10200;
        public const int CREEL_APP_NOT_AUTHORIZE_TO_PERFORM_TASK_CODE = 1200; 
        #endregion
    }
}