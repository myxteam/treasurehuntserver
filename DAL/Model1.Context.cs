﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TreasureGameWeb.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Model1Container : DbContext
    {
        public Model1Container()
            : base("name=Model1Container")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<User> Users { get; set; }
        public DbSet<Session> Sessions { get; set; }
        public DbSet<Grid> Grids { get; set; }
        public DbSet<Game_Event_Logs> Game_Event_Logs { get; set; }
        public DbSet<Debug_Log> Debug_Log { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<Trading_Log_Specific> Trading_Log_Specific { get; set; }
        public DbSet<Exchange_Log> Exchange_Log { get; set; }
        public DbSet<Device_per_User> Device_per_User { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<Balance> Balances { get; set; }
    }
}
