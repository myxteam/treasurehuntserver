
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 08/14/2015 11:29:59
-- Generated from EDMX file: C:\Users\dominicy.T2\Source\Workspaces\TreasureHunt\TreasureHuntServer\TimeZappWeb-branch\TimeZappWeb\TreasureGameWeb\DAL\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TreasureHuntGame];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_UserSession]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Sessions] DROP CONSTRAINT [FK_UserSession];
GO
IF OBJECT_ID(N'[dbo].[FK_GridSession]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Grids] DROP CONSTRAINT [FK_GridSession];
GO
IF OBJECT_ID(N'[dbo].[FK_SessionGameEvent]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Game_Event_Logs] DROP CONSTRAINT [FK_SessionGameEvent];
GO
IF OBJECT_ID(N'[dbo].[FK_TradingLogsSpecificExchange_Logs]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Trading_Log_Specific] DROP CONSTRAINT [FK_TradingLogsSpecificExchange_Logs];
GO
IF OBJECT_ID(N'[dbo].[FK_UserExchange_Log]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Exchange_Log] DROP CONSTRAINT [FK_UserExchange_Log];
GO
IF OBJECT_ID(N'[dbo].[FK_UserDevice_per_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Device_per_User] DROP CONSTRAINT [FK_UserDevice_per_User];
GO
IF OBJECT_ID(N'[dbo].[FK_DeviceDevice_per_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Device_per_User] DROP CONSTRAINT [FK_DeviceDevice_per_User];
GO
IF OBJECT_ID(N'[dbo].[FK_UserBalance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Balances] DROP CONSTRAINT [FK_UserBalance];
GO
IF OBJECT_ID(N'[dbo].[FK_CurrencyBalance]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Balances] DROP CONSTRAINT [FK_CurrencyBalance];
GO
IF OBJECT_ID(N'[dbo].[FK_Exchange_Rate_inherits_Currency]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Currencies_Exchange_Rate] DROP CONSTRAINT [FK_Exchange_Rate_inherits_Currency];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Sessions]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Sessions];
GO
IF OBJECT_ID(N'[dbo].[Grids]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Grids];
GO
IF OBJECT_ID(N'[dbo].[Game_Event_Logs]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Game_Event_Logs];
GO
IF OBJECT_ID(N'[dbo].[Debug_Log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Debug_Log];
GO
IF OBJECT_ID(N'[dbo].[Devices]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Devices];
GO
IF OBJECT_ID(N'[dbo].[Trading_Log_Specific]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Trading_Log_Specific];
GO
IF OBJECT_ID(N'[dbo].[Exchange_Log]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Exchange_Log];
GO
IF OBJECT_ID(N'[dbo].[Device_per_User]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Device_per_User];
GO
IF OBJECT_ID(N'[dbo].[Currencies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Currencies];
GO
IF OBJECT_ID(N'[dbo].[Balances]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Balances];
GO
IF OBJECT_ID(N'[dbo].[Currencies_Exchange_Rate]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Currencies_Exchange_Rate];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Username] nvarchar(max)  NULL,
    [Password] nvarchar(max)  NULL,
    [BirthDay] datetime  NULL,
    [GUID] uniqueidentifier  NOT NULL,
    [Image] tinyint  NULL,
    [CreelGUID] uniqueidentifier  NULL,
    [Displayname] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Sessions'
CREATE TABLE [dbo].[Sessions] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Created] datetime  NOT NULL,
    [Finished] datetime  NULL,
    [LastUpdate] datetime  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'Grids'
CREATE TABLE [dbo].[Grids] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Rows] int  NOT NULL,
    [Collums] int  NOT NULL,
    [SetupString] nvarchar(max)  NOT NULL,
    [Sessions_Id] int  NOT NULL
);
GO

-- Creating table 'Game_Event_Logs'
CREATE TABLE [dbo].[Game_Event_Logs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Message] nvarchar(max)  NOT NULL,
    [Session_Id] int  NOT NULL
);
GO

-- Creating table 'Debug_Log'
CREATE TABLE [dbo].[Debug_Log] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Message] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Devices'
CREATE TABLE [dbo].[Devices] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Mac_Id] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'Trading_Log_Specific'
CREATE TABLE [dbo].[Trading_Log_Specific] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Bought_Currency] nvarchar(max)  NOT NULL,
    [Bought_Amount] int  NOT NULL,
    [Sold_Currency] nvarchar(max)  NOT NULL,
    [Sold_Amount] int  NOT NULL,
    [Exchange_Logs_Id] int  NOT NULL
);
GO

-- Creating table 'Exchange_Log'
CREATE TABLE [dbo].[Exchange_Log] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Date] datetime  NOT NULL,
    [Message] nvarchar(max)  NOT NULL,
    [UserExchange_Log_Exchange_Log_Id] int  NOT NULL
);
GO

-- Creating table 'Device_per_User'
CREATE TABLE [dbo].[Device_per_User] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [DeviceId] int  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'Currencies'
CREATE TABLE [dbo].[Currencies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'Balances'
CREATE TABLE [dbo].[Balances] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Amount] float  NOT NULL,
    [CurrencyId] int  NOT NULL,
    [User_Id] int  NOT NULL
);
GO

-- Creating table 'Currencies_Exchange_Rate'
CREATE TABLE [dbo].[Currencies_Exchange_Rate] (
    [SellName] nvarchar(max)  NOT NULL,
    [SellPrice] float  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Sessions'
ALTER TABLE [dbo].[Sessions]
ADD CONSTRAINT [PK_Sessions]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Grids'
ALTER TABLE [dbo].[Grids]
ADD CONSTRAINT [PK_Grids]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Game_Event_Logs'
ALTER TABLE [dbo].[Game_Event_Logs]
ADD CONSTRAINT [PK_Game_Event_Logs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Debug_Log'
ALTER TABLE [dbo].[Debug_Log]
ADD CONSTRAINT [PK_Debug_Log]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Devices'
ALTER TABLE [dbo].[Devices]
ADD CONSTRAINT [PK_Devices]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Trading_Log_Specific'
ALTER TABLE [dbo].[Trading_Log_Specific]
ADD CONSTRAINT [PK_Trading_Log_Specific]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Exchange_Log'
ALTER TABLE [dbo].[Exchange_Log]
ADD CONSTRAINT [PK_Exchange_Log]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Device_per_User'
ALTER TABLE [dbo].[Device_per_User]
ADD CONSTRAINT [PK_Device_per_User]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Currencies'
ALTER TABLE [dbo].[Currencies]
ADD CONSTRAINT [PK_Currencies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Balances'
ALTER TABLE [dbo].[Balances]
ADD CONSTRAINT [PK_Balances]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Currencies_Exchange_Rate'
ALTER TABLE [dbo].[Currencies_Exchange_Rate]
ADD CONSTRAINT [PK_Currencies_Exchange_Rate]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [User_Id] in table 'Sessions'
ALTER TABLE [dbo].[Sessions]
ADD CONSTRAINT [FK_UserSession]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserSession'
CREATE INDEX [IX_FK_UserSession]
ON [dbo].[Sessions]
    ([User_Id]);
GO

-- Creating foreign key on [Sessions_Id] in table 'Grids'
ALTER TABLE [dbo].[Grids]
ADD CONSTRAINT [FK_GridSession]
    FOREIGN KEY ([Sessions_Id])
    REFERENCES [dbo].[Sessions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GridSession'
CREATE INDEX [IX_FK_GridSession]
ON [dbo].[Grids]
    ([Sessions_Id]);
GO

-- Creating foreign key on [Session_Id] in table 'Game_Event_Logs'
ALTER TABLE [dbo].[Game_Event_Logs]
ADD CONSTRAINT [FK_SessionGameEvent]
    FOREIGN KEY ([Session_Id])
    REFERENCES [dbo].[Sessions]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SessionGameEvent'
CREATE INDEX [IX_FK_SessionGameEvent]
ON [dbo].[Game_Event_Logs]
    ([Session_Id]);
GO

-- Creating foreign key on [Exchange_Logs_Id] in table 'Trading_Log_Specific'
ALTER TABLE [dbo].[Trading_Log_Specific]
ADD CONSTRAINT [FK_TradingLogsSpecificExchange_Logs]
    FOREIGN KEY ([Exchange_Logs_Id])
    REFERENCES [dbo].[Exchange_Log]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TradingLogsSpecificExchange_Logs'
CREATE INDEX [IX_FK_TradingLogsSpecificExchange_Logs]
ON [dbo].[Trading_Log_Specific]
    ([Exchange_Logs_Id]);
GO

-- Creating foreign key on [UserExchange_Log_Exchange_Log_Id] in table 'Exchange_Log'
ALTER TABLE [dbo].[Exchange_Log]
ADD CONSTRAINT [FK_UserExchange_Log]
    FOREIGN KEY ([UserExchange_Log_Exchange_Log_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserExchange_Log'
CREATE INDEX [IX_FK_UserExchange_Log]
ON [dbo].[Exchange_Log]
    ([UserExchange_Log_Exchange_Log_Id]);
GO

-- Creating foreign key on [UserId] in table 'Device_per_User'
ALTER TABLE [dbo].[Device_per_User]
ADD CONSTRAINT [FK_UserDevice_per_User]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserDevice_per_User'
CREATE INDEX [IX_FK_UserDevice_per_User]
ON [dbo].[Device_per_User]
    ([UserId]);
GO

-- Creating foreign key on [DeviceId] in table 'Device_per_User'
ALTER TABLE [dbo].[Device_per_User]
ADD CONSTRAINT [FK_DeviceDevice_per_User]
    FOREIGN KEY ([DeviceId])
    REFERENCES [dbo].[Devices]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DeviceDevice_per_User'
CREATE INDEX [IX_FK_DeviceDevice_per_User]
ON [dbo].[Device_per_User]
    ([DeviceId]);
GO

-- Creating foreign key on [User_Id] in table 'Balances'
ALTER TABLE [dbo].[Balances]
ADD CONSTRAINT [FK_UserBalance]
    FOREIGN KEY ([User_Id])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserBalance'
CREATE INDEX [IX_FK_UserBalance]
ON [dbo].[Balances]
    ([User_Id]);
GO

-- Creating foreign key on [CurrencyId] in table 'Balances'
ALTER TABLE [dbo].[Balances]
ADD CONSTRAINT [FK_CurrencyBalance]
    FOREIGN KEY ([CurrencyId])
    REFERENCES [dbo].[Currencies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CurrencyBalance'
CREATE INDEX [IX_FK_CurrencyBalance]
ON [dbo].[Balances]
    ([CurrencyId]);
GO

-- Creating foreign key on [Id] in table 'Currencies_Exchange_Rate'
ALTER TABLE [dbo].[Currencies_Exchange_Rate]
ADD CONSTRAINT [FK_Exchange_Rate_inherits_Currency]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Currencies]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------