//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TreasureGameWeb.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Grid
    {
        public int Id { get; set; }
        public int Rows { get; set; }
        public int Collums { get; set; }
        public string SetupString { get; set; }
    
        public virtual Session Sessions { get; set; }
    }
}
