﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TreasureGameWeb.DAL.Repository
{
    public static class LogRepository
    {

        /// <summary>
        /// Creates Exchange_Log entry
        /// </summary>
        /// <param name="offerCurrency">Currency name offered in trading</param>
        /// <param name="boughtCurrency">Currency name bought in trading</param>
        /// <param name="offerAmount">Offered amount in trading</param>
        /// <param name="boughtAmount">Bought amount in trading</param>
        /// <returns></returns>
        public static Exchange_Log CreateTradingLog(string offerCurrency, string boughtCurrency, int offerAmount, int boughtAmount)
        {

            Exchange_Log logUpdate = new Exchange_Log();
            logUpdate.Date = DateTime.UtcNow;
            logUpdate.Message = @"Traded " + offerAmount + " " + offerCurrency + " for " +
                                boughtAmount + " " + boughtCurrency;
            logUpdate.TradingLogsSpecific = new Trading_Log_Specific()
            {
                Bought_Amount = boughtAmount,
                Bought_Currency = boughtCurrency,
                Sold_Amount = offerAmount,
                Sold_Currency = offerCurrency
            };

            return logUpdate;
        }
       
    }
}