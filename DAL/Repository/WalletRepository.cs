﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TreasureGameModels;

namespace TreasureGameWeb.DAL.Repository
{
    public static class WalletRepository
    {
        public static List<Balance> GetUserWallet(Guid userGUID)
        {
            using (Model1Container context = new Model1Container())
            {
                var userID = context.Users.Where(x => x.GUID == userGUID).Select(x => x.Id).FirstOrDefault();
                return context.Balances.Include("User").Where(x => x.User.Id == userID).ToList();
            }
        }

        public static void TradeCurrencies(int offerBalanceID, int boughtBalanceID, int offerAmount, int boughtAmount)
        {
            using (Model1Container context = new Model1Container())
            {
                var offered = context.Balances.Where(x => x.Id == offerBalanceID).FirstOrDefault();
                var bought = context.Balances.Where(x => x.Id == boughtBalanceID).FirstOrDefault();

                bought.Amount = bought.Amount + boughtAmount;
                offered.Amount = offered.Amount - offerAmount;

                var logUpdate = LogRepository.CreateTradingLog(((GameCurrencies)offered.CurrencyId).ToString(), 
                    ((GameCurrencies)bought.CurrencyId).ToString(), offerAmount, boughtAmount);
                
                context.Exchange_Log.Add(logUpdate);
                context.Trading_Log_Specific.Add(logUpdate.TradingLogsSpecific);
                context.SaveChanges();
            }
        }
    }
}