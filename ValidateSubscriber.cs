﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using TreasureGameWeb.DAL;

namespace TimeZappWeb
{
    public class ValidateSubscriber : System.Web.Http.AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
               if (! (actionContext.Request.Headers.Contains("Username") ||  actionContext.Request.Headers.Contains("Password")))
               {
                   HttpContext.Current.Response.AddHeader("authenticationToken", "");
                   HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
                   base.OnAuthorization(actionContext);
                   return;
               }
            // user with username/password
               if (actionContext.Request.Headers.Contains("Username")  && actionContext.Request.Headers.Contains("Password") )
               {
                   // get value from header
                   string username = Convert.ToString(actionContext.Request.Headers.GetValues("Username").FirstOrDefault());
                   string password = Convert.ToString(actionContext.Request.Headers.GetValues("Password").FirstOrDefault());

                   Model1Container TZE = new Model1Container();

                   User user = TZE.Users.Where(t => t.Username == username && t.Password == password).FirstOrDefault();
                   if (user != null)
                   {
                       HttpContext.Current.Response.AddHeader("authenticationToken", username);
                       HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
                     //  user..LastActivity = DateTime.UtcNow;
                       TZE.SaveChanges();
                       return;
                   }
                   else
                   {
                       HttpContext.Current.Response.AddHeader("authenticationToken", username);
                       HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
                       //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                       base.OnAuthorization(actionContext);
                       return;
                   }
               }
               // user with GUID key (trial user, before signup), sends guid as Username
               else if (actionContext.Request.Headers.Contains("Username"))
               {
                  // get value from header
                  string username = Convert.ToString(actionContext.Request.Headers.GetValues("Username").FirstOrDefault());
                  Model1Container TZE = new Model1Container();

                   User user = TZE.Users.Where(t => t.GUID.ToString() == username ).FirstOrDefault();
                //   if (user != null && user.Role == 5 && user.Status != 0)
                   if(user != null)
                   {
                       //if (user.UserName == null)
                       //{
                           HttpContext.Current.Response.AddHeader("authenticationToken", username);
                           //  actionContext.Request.Headers.Add("Password", "Trial");
                           HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
                           return;
                       //}
                       //else
                       //{
                       //    HttpContext.Current.Response.AddHeader("authenticationToken", username);
                       //    HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
                       //    //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                       //    base.OnAuthorization(actionContext);
                       //    return;
                       //}
                   }
                   else
                   {
                       HttpContext.Current.Response.AddHeader("authenticationToken", username);
                       HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
                       //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
                       base.OnAuthorization(actionContext);
                       return;

                   }
               }
            //actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.ExpectationFailed);
            actionContext.Response.ReasonPhrase = "Please provide valid inputs";
            base.OnAuthorization(actionContext);
        }

        public static class UserRoles
        {
            public static String SuperAdmin = "SuperAdmin";
            public static String Admin = "Admin";
            public static String JuniorAdmin = "JuniorAdmin";
            public static String ExternalAdmin = "ExternalAdmin";
            public static String User = "User";
            public static String TrialUser = "TrialUser";

            public static short Role2Short(String role)
            {
                switch(role)
                {
                    case "SuperAdmin": {return 0;}
                    case "Admin": {return 1;}
                    case "JuniorAdmin": {return 2;}
                    case "ExternalAdmin": {return 3;}
                    case "User": {return 4;}
                    case "TrialUser": {return 5;}
                }
                return -1;
            }

            public static String Short2Role(short RoleId)
            {
                switch (RoleId)
                {
                    case 0: { return "SuperAdmin"; }
                    case 1: { return "Admin"; }
                    case 2: { return "JuniorAdmin"; }
                    case 3: { return "ExternalAdmin"; }
                    case 4: { return "User"; }
                    case 5: { return "TrialUser"; }
                }
                return "Error";
            }
        }
        
    }
}