﻿using DominionErrorLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreasureGameModels;
using TreasureGameWeb.Class;
using TreasureGameWeb.DAL;
using TreasureGameWeb.Models.Internal;

namespace TreasureGameWeb.Controllers
{
    public class Session2Controller : ApiController
    {
        [Route("api/Session2/GetSession")]
        [HttpPost]
        public ApiResult GetSession(LogInModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    if (model.UserGuid != null)
                    {
                        user = TZE.Users.Where(t => t.GUID == model.UserGuid).FirstOrDefault();

                        //var macValid = user.Devices.Where(d => d.Mac_Id == model.MacAddress).FirstOrDefault();
                        //if (macValid == null)
                        //{
                        //    result.Status = ApiResponse.OTHER_ERROR;
                        //    result.ErrorString = "Invalid Device";
                        //    return result;
                        //}
                    }
                    var rnd = new Random(DateTime.Now.Millisecond);

                    Session sess = new Session();
                    sess.User = user;
                    sess.Created = DateTime.UtcNow;
                    sess.LastUpdate = DateTime.UtcNow;
                    Grid grid = new Grid();
                    grid.Collums = 4;
                    grid.Rows = 3;

                    #region setup
                    for (int i = 0; i < grid.Collums; i++)
                    {
                        for (int j = 0; j < grid.Rows; j++)
                        {
                            String str = "";
                            if (model.GameID.ToString() == ConstanStrings.THGameId)
                            {
                                RandomChestGrider(GameCurrenciesConstant.GOLD, GameCurrenciesConstant.SILVER, GameCurrenciesConstant.DOUBLOON, GameCurrenciesConstant.COPPER, rnd, out str);
                            }
                            else
                            {
                                result.Status = ApiResponse.OTHER_ERROR;
                                result.ErrorString = ConstantStringsMessages.INVALID_ID;
                                return result;
                            }

                            grid.SetupString += str;
                        }
                    }
                    #endregion

                    sess.Grid = grid;
                    TZE.Sessions.Add(sess);
                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;
                    result.Data = sess.Id + ":" + grid.Collums + ":" + grid.Rows + ":" + grid.SetupString;
                    return result;
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME_CREEL, true, e);
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                    return result;
                }
            }
        }
        public void RandomChestGrider(string firstCurenncy, string secondCurrency, string thirdCurrency, string fourthCurrency,Random rnd, out string str)
        {
            string strGold = "";
            int ticks = rnd.Next(0, 100) + 1;
            if (ticks >= 65)
            {
                strGold += firstCurenncy + ",";
                int ticks2 = rnd.Next(0, 100) + 1;
                if (ticks2 <= 75) strGold += "5:";
                else if (ticks2 <= 85) strGold += "10:";
                else strGold += "50:";
            }          
            else
            {
                strGold += "NULL,0:";
            }
            str = strGold;
        }
    }
}
