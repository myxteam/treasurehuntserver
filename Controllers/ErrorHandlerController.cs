﻿using DominionErrorLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreasureGameWeb.Class;

namespace TreasureGameWeb.Controllers
{

    public class ErrorHandlerController : ApiController
    {
        [Route("api/ErrorHandler/SendError")]
        [HttpPost]
        public bool SendError(Exception e)
        {
            try
            {
                ErrorSender errorSender = new ErrorSender();
                errorSender.UploadError(ConstantStringsMessages.APPLICATION_NAME, false, e);
            }
            catch (Exception ex)
            {
                ErrorSender errorSender = new ErrorSender();
                errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, ex);
            }
  

            return true;
        }
    }
}
