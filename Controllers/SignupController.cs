﻿using DominionErrorLibrary;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using TreasureGameModels;
using TreasureGameWeb.Class;
using TreasureGameWeb.DAL;
using TreasureGameWeb.Models.Internal;

namespace TreasureGameWeb.Controllers
{
    public class SignupController : ApiController
    {
        /*
         * Remarks
         * Might Use for Future Plans
         */
        [Route("api/Signup/SignUp")]
        [HttpPost]
        public ApiResult SignUp(LogInModel model)
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            //Debug.WriteLine("Signup Start ");
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                try
                {
                    if (TZE.Users.Where(t => t.Username == model.UserName).Count() != 0)
                    {
                        //stopwatch.Stop();

                        //Debug.WriteLine("Signup  return other error 1 " + stopwatch.Elapsed);
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.USERNAME_ALREADY_TAKEN;
                        return result;
                    }
                    DAL.Device device = new DAL.Device();
                    DAL.User user = new DAL.User();
                    user.Balances = new List<Balance>();


                    if (model.UserGuid == null || model.UserGuid == Guid.Empty)
                        user.GUID = Guid.NewGuid();
                    else
                    {
                        user = TZE.Users.Where(t => t.GUID == model.UserGuid).FirstOrDefault();
                        user.GUID = model.UserGuid;
                    }

                    user.CreelGUID = Guid.Empty;
                   

                    //int currencyId = GameCurrenciesConstant.GOLD_ID;
                    TreasureGameWeb.DAL.Balance e1 = new DAL.Balance();
                    e1.CurrencyId = GameCurrenciesConstant.GOLD_ID;

                    if(model.Amount != null)
                        e1.Amount = 200+ model.Amount;
                    else
                        e1.Amount = 200;
                    user.Balances.Add(e1);

                   
                    user.Displayname = "red";
                    user.Username = model.UserName;
                    using (MD5 md5Hash = MD5.Create())
                    {
                        string hash = GetMd5Hash(md5Hash, model.Password);
                        user.Password = hash;
                    }
                    TZE.Users.Add(user);
                    TZE.SaveChanges();

                    result.Status = ApiResponse.OK;
                    result.Data = user.GUID.ToString();
                    //stopwatch.Stop();

                    //Debug.WriteLine("Signup ok " + stopwatch.Elapsed);
                    return result;
                }
                catch (DbEntityValidationException e)
                {
                    //Debug_Log log = new Debug_Log();
                    //log.Date = DateTime.UtcNow;
                    //log.Message = e.Message + " " + e.StackTrace;
                    //TZE.Debug_Log.Add(log);
                    //TZE.SaveChanges();
                    //stopwatch.Stop();

                    //Debug.WriteLine("Signup  return other error 2 " + stopwatch.Elapsed);
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, e);

                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                    return result;
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, e);

                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                    return result;
                }
            }

        }
        [Route("api/Signup/SignUpDemo")]
        [HttpPost]
        public ApiResult SignUpDemo(LogInModel model)
        {
            //Stopwatch stopwatch = new Stopwatch();
            //stopwatch.Start();
            //Debug.WriteLine("Signup Start ");
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                try
                {
                    string newuserName = model.UserGuid.ToString();
                    if (TZE.Users.Where(t => t.Username == newuserName).Count() != 0)
                    {
                        result.Status = ApiResponse.OK;
                        result.Data = newuserName;
                        return result;
                    }
                    DAL.Device device = new DAL.Device();
                    DAL.User user = new DAL.User();
                    user.Balances = new List<Balance>();

                    user.GUID = new Guid();

                    user.GUID = model.UserGuid;
                    user.CreelGUID = Guid.Empty;

                    TreasureGameWeb.DAL.Balance e1 = new DAL.Balance();
                    e1.CurrencyId = GameCurrenciesConstant.GOLD_ID;

                    e1.Amount = 200;

                    user.Balances.Add(e1);                  

                    user.Displayname = "red";
                    user.Username = model.UserGuid+"";
                    using (MD5 md5Hash = MD5.Create())
                    {
                        string hash = GetMd5Hash(md5Hash, model.UserGuid+"");
                        user.Password = hash;
                    }
                    TZE.Users.Add(user);
                    TZE.SaveChanges();

                    result.Status = ApiResponse.OK;
                    result.Data = user.GUID.ToString();

                    return result;
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, e);
                    Debug.WriteLine(e.InnerException);
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                    return result;
                }
            }

        }

        string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}
