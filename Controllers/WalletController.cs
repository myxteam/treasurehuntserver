﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading;
using TreasureGameModels;
using TreasureGameWeb.DAL;
using TreasureGameWeb.Models.Internal;
using System.Diagnostics;
using TreasureGameWeb.Class;
using DominionErrorLibrary;
/*
 * Remarks
 * Might Use for Future Plans
 */
namespace TreasureGameWeb.Controllers
{
    public class WalletController : ApiController
    {
        [Route("api/Wallet/GetOverallBalance")]
        [HttpPost]
        public ApiResult GetOverallBalance(LogInModel lm) {

            try
            {           
                using (Model1Container context = new Model1Container())
                {
                    var userId = context.Users.Where(x => x.GUID == lm.UserGuid).FirstOrDefault().Id;
                    var currencies = GetUserCurrencies(userId);

                    Debug.WriteLine("User ID wallet" + lm.UserGuid);
                    if (currencies.Count < 1)
                    {

                        return new ApiResult()
                        {
                            Status = ApiResponse.OTHER_ERROR,
                            ErrorString = ConstantStringsMessages.WALLET_NO_CURRENCY_FOUND
                        };
                    }
                    else
                    {

                        WalletModel wallet = new WalletModel();
                        wallet.Currencies = currencies;

                        return new ApiResult()
                        {
                            Data = wallet,
                            Status = ApiResponse.OK
                        };
                    }
                }
            }
            catch (Exception e)
            {
                ErrorSender errorSender = new ErrorSender();
                errorSender.UploadError(ConstantStringsMessages.SERVER_NAME_CREEL, true, e);
                return new ApiResult()
                {
                    Status = ApiResponse.OTHER_ERROR,
                    ErrorString = ConstantStringsMessages.NO_USER_FOUND
                };
            }
        }

        [Route("api/Wallet/GetExchangeRate")]
        [HttpPost]
        public ApiResult GetExchangeRate(string boughtCurrency, string offerCurrency)
        {
            int coinRate = 0;

            switch (offerCurrency)
            {
                case GameCurrenciesConstant.GOLD:
                    if (boughtCurrency == GameCurrenciesConstant.SILVER) coinRate = ConstanStrings.GoldToSilver;
                    else if (boughtCurrency == GameCurrenciesConstant.COPPER) coinRate = ConstanStrings.GoldToCopper;
                    else if (boughtCurrency == GameCurrenciesConstant.DOUBLOON) coinRate = ConstanStrings.GoldToDubloons;
                break;
                case GameCurrenciesConstant.SILVER:
                if (boughtCurrency == GameCurrenciesConstant.COPPER) coinRate = ConstanStrings.SilverToCopper;
                else if (boughtCurrency == GameCurrenciesConstant.DOUBLOON) coinRate = ConstanStrings.SilverToDubloons;
                break;
                case GameCurrenciesConstant.COPPER:
                if (boughtCurrency == GameCurrenciesConstant.DOUBLOON) coinRate = ConstanStrings.CopperToDubloons;
                break;
                case GameCurrenciesConstant.DOUBLOON:
                break;
            }
                       
            return new ApiResult()
            {
               Data = coinRate,
               Status = ApiResponse.OK
            };
        }

        [Route("api/Wallet/TradeCurrencies")]
        [HttpPost]
        public ApiResult TradeCurrencies(AuthenticatedTradingModel aTM) {

            using (Model1Container context = new Model1Container())
            {
                var user = context.Users.Where(x => x.GUID == aTM.UserGuid).FirstOrDefault();
                var userId = user.Id;
                var userWallet = GetUserCurrencies(userId);

                var userOfferedBalance = userWallet.Where(x => x.CurrencyName == aTM.tm.OfferCurrency).FirstOrDefault();
                if (userOfferedBalance.Amount >= aTM.tm.OfferAmount)
                {
                    var userBoughtBalance = userWallet.Where(x => x.CurrencyName == aTM.tm.BoughtCurrency).FirstOrDefault();
                    if (userBoughtBalance == null)
                    {
                        return new ApiResult()
                        {
                            Status = ApiResponse.OTHER_ERROR,
                            ErrorString = aTM.tm.BoughtCurrency + " not found in User."
                        };
                    }
                    else {

                        var offered = context.Balances.Where(x => x.Id == userOfferedBalance.BalanceID).FirstOrDefault();
                        var bought = context.Balances.Where(x => x.Id == userBoughtBalance.BalanceID).FirstOrDefault();

                        bought.Amount = bought.Amount + aTM.tm.BoughtAmount;
                        offered.Amount = offered.Amount - aTM.tm.OfferAmount;

                        Exchange_Log logUpdate = new Exchange_Log();
                        logUpdate.Date = DateTime.UtcNow;
                        logUpdate.Message = @"Traded " + aTM.tm.OfferAmount + " " + aTM.tm.OfferCurrency + " for " +
                                            aTM.tm.BoughtAmount + " " + aTM.tm.BoughtCurrency;
                        logUpdate.TradingLogsSpecific = new Trading_Log_Specific()
                        {
                            Bought_Amount = aTM.tm.BoughtAmount,
                            Bought_Currency = aTM.tm.BoughtCurrency,
                            Sold_Amount = aTM.tm.OfferAmount,
                            Sold_Currency = aTM.tm.OfferCurrency
                            
                        };

                        //context.Exchange_Log.Add(logUpdate);
                        user.Exchange_Log.Add(logUpdate);
                        context.SaveChanges();
                        return new ApiResult()
                        {
                            Data = "Traded currencies successfully.",
                            Status = ApiResponse.OK
                        };
                    }
                }
                else return new ApiResult()
                {
                    Status = ApiResponse.OTHER_ERROR,
                    ErrorString = "Not enough coins to trade."
                };
            }
        
        }

        private List<WalletModelEntry> GetUserCurrencies(int userID)
        {
            using (Model1Container context = new Model1Container())
            {
                List<WalletModelEntry> walletList = new List<WalletModelEntry>();
                var wallet = context.Balances.Include("User").Where(x => x.User.Id == userID).ToList().Where(c=> c.CurrencyId == GameCurrenciesConstant.GOLD_ID).FirstOrDefault();

                //foreach (var currency in wallet)
                //{
                //    WalletModelEntry entry = new WalletModelEntry();
                //    entry.BalanceID = currency.Id;
                //    entry.CurrencyName = context.Currencies.Where(x => x.Id == currency.CurrencyId).Select(x => x.Name).FirstOrDefault();
                //    entry.Amount = currency.Amount;
                //    walletList.Add(entry);
                //}
                WalletModelEntry entry = new WalletModelEntry();
                entry.BalanceID = wallet.Id;
                 entry.CurrencyName = GameCurrenciesConstant.GOLD;
                 entry.Amount = wallet.Amount;
                 walletList.Add(entry);
                return walletList;
            }
        }

        private double GetCurrentAmount(int userID, int currencyID)
        {
            using (Model1Container context = new Model1Container())
            {
                return context.Balances.Include("User").Where(x => x.User.Id == userID && x.CurrencyId == currencyID).Select(x => x.Amount).FirstOrDefault();            
            }
        
        }
    }
}
