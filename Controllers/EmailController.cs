﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreasureGameModels;
using System.Net.Mail;

namespace TreasureGameWeb.Controllers
{
    public class EmailController : ApiController
    {
        private const string TH_MAIL = "support@email.com";

        public ApiResult Activate() {
            
            return null;
        }

        private void CreateMail(string[] mailList) {
            MailMessage mail = new MailMessage(TH_MAIL, TH_MAIL);
            mail.Subject = "Account Activation";
            mail.Body = @"Thank you for registering in Treasure Hunt!

                          Click the link to activate your account: ";
            SmtpClient client = new SmtpClient("");
            client.UseDefaultCredentials = true;
            client.Send(mail);            
        }
    }
}
