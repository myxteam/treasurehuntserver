﻿using Creel.CustomerWorkspace;
using Creel.Exceptions;
using DominionErrorLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreasureGameModels;
using TreasureGameWeb.Class;
using TreasureGameWeb.DAL;

namespace TreasureGameWeb.Controllers
{
    public class CreelController : ApiController
    {
        public const string SERVICE_CONSUMER_KEY = "00000000-0000-0000-0000-000000000000";
        public const string SERVICE_CONSUMER_SECRET = "000000000000000000000000000000000000000000000000";
        public static string SERVER_ID = "869312A6-FD5A-4DCF-84BF-5079F8E62800"; // sandbox       
        private Guid _treasureCorpMerchantAccountId = new Guid("00000000-0000-0000-0000-000000000000");
        private Guid _treasureHuntCurrencyId = new Guid("00000000-0000-0000-000000000000");
        public const string CREEL_WEBSITE = "http://customers.creel.me/";
      
        [Route("api/Creel/GetCurrentCapcity_Creel")]
        [HttpPost]
        public ApiResult GetCurrentCapcity_Creel(TransferModel model)
        {
            ApiResult result = new ApiResult();
            try
            {
                ConsumerDebitAccountManager getCurrentSpendingCapacity = new ConsumerDebitAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                var creelServerResponse = getCurrentSpendingCapacity.GetConsumerDebitAccountAvailableBalanceByCustomerAccountIdMerchantAccountIdAndCurrencyId(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId);

                result.Status = ApiResponse.OK;
                result.Data = creelServerResponse;
                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/GetTotalSpendingCapacity_Creel")]
        [HttpPost]
        public ApiResult GetTotalSpendingCapacity_Creel(TransferModel model)
        {
            ApiResult result = new ApiResult();
            try
            {
                ConsumerDebitAccountManager getCurrentSpendingCapacity = new ConsumerDebitAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                var creelServerResponseForGetCurrenctSpendingCapacity = getCurrentSpendingCapacity.GetConsumerDebitAccountAvailableBalanceByCustomerAccountIdMerchantAccountIdAndCurrencyId(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId);

                CustomerAccountManager getTotalSpendingCapacity = new CustomerAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                var creelServerResponse = getTotalSpendingCapacity.GetTotalSpendingCapacityByCurrencyId(model.CreelGuid, _treasureHuntCurrencyId);
                
                result.Status = ApiResponse.OK;
                result.Data = (creelServerResponse - creelServerResponseForGetCurrenctSpendingCapacity);
                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);

            }
        }


        [Route("api/Creel/IntelligentBuyFromCreel")]
        [HttpPost]
        public ApiResult IntelligentBuyFromCreel(TransferModel model)
        {
            ApiResult result = new ApiResult();
            Guid transferFromCreelRequestId;
            BuyOrderManager creelManagerBuyOrTransfer;

            try
            {
                TransferFromCreelRequestManager transferFromCreelRequestsManager = new TransferFromCreelRequestManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                transferFromCreelRequestId = transferFromCreelRequestsManager.AddNewTransferFromCreelRequest(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId, model.Amount, string.Empty);



                ConsumerDebitAccountManager getTotalSpendingCapacity = new ConsumerDebitAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                var creelServerResponse = getTotalSpendingCapacity.GetConsumerDebitAccountAvailableBalanceByCustomerAccountIdMerchantAccountIdAndCurrencyId(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId);
                
                creelManagerBuyOrTransfer = new BuyOrderManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                transferFromCreelRequestId = creelManagerBuyOrTransfer.AddNewBuyOrder(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId, model.Amount, string.Empty, string.Empty);

                result = changeIngameCurrency(model, true);

                if (result.Status != ApiResponse.OTHER_ERROR || result.Status != ApiResponse.NETWORK_ERROR)
                {
                    result.Status = ApiResponse.OK;
                    result.Data = transferFromCreelRequestId;
                }

                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/BuyFromCreel")]
        [HttpPost]
        public ApiResult BuyFromCreel(TransferModel model)
        {
            ApiResult result = new ApiResult();
            Guid transferFromCreelRequestId;
            BuyOrderManager creelManagerBuyOrTransfer;

            try
            {
                ConsumerDebitAccountManager getTotalSpendingCapacity = new ConsumerDebitAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                var creelServerResponse = getTotalSpendingCapacity.GetConsumerDebitAccountAvailableBalanceByCustomerAccountIdMerchantAccountIdAndCurrencyId(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId);
                creelManagerBuyOrTransfer = new BuyOrderManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                transferFromCreelRequestId = creelManagerBuyOrTransfer.AddNewBuyOrder(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId, model.Amount, string.Empty, string.Empty);

                result = changeIngameCurrency(model, true);

                if (result.Status != ApiResponse.OTHER_ERROR || result.Status != ApiResponse.NETWORK_ERROR)
                {
                    result.Status = ApiResponse.OK;
                    result.Data = transferFromCreelRequestId;
                }

                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/TransferCurrencyFromCreel")]
        [HttpPost]
        public ApiResult TransferCurrencyFromCreel(TransferModel model)
        {
            ApiResult result = new ApiResult();
            try
            {
                TransferFromCreelRequestManager transferFromCreelRequestsManager = new TransferFromCreelRequestManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                Guid transferFromCreelRequestId = transferFromCreelRequestsManager.AddNewTransferFromCreelRequest(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId, model.Amount, string.Empty);
                result = changeIngameCurrency(model, true);

                if (result.Status != ApiResponse.OTHER_ERROR || result.Status != ApiResponse.NETWORK_ERROR)
                {
                    result.Status = ApiResponse.OK;
                    result.Data = transferFromCreelRequestId;
                }
                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/TransferCurrencyToCreel")]
        [HttpPost]
        public ApiResult TransferCurrencyToCreel(TransferModel model)
        {
            ApiResult result = new ApiResult();

            try
            {
                SellOrderManager sellOrderManager = new SellOrderManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                Guid transferToCreelRequestId = Guid.Empty;

                transferToCreelRequestId = sellOrderManager.AddNewSellOrder(model.CreelGuid, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId, model.Amount, string.Empty, string.Empty);

                result = changeIngameCurrency(model, false);

                if (result.Status != ApiResponse.OTHER_ERROR || result.Status != ApiResponse.NETWORK_ERROR)
                {
                    result.Status = ApiResponse.OK;
                    result.Data = transferToCreelRequestId;
                }
                else
                {
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                }
                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/TransferToUser")]
        [HttpPost]
        public ApiResult TransferToUser(TransferToUserModel model)
        {
            ApiResult result = new ApiResult();
            try
            {
                using (Model1Container TZE = new Model1Container())
                {
                    var user = TZE.Users.Where(t => t.GUID == model.USER_GUID).FirstOrDefault();
                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.INVALID_USER;
                        return result;
                    }
                    SendOrderManager creelServerResponse = new SendOrderManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceID);
                    creelServerResponse.AddNewSendOrder(model.FROM_CREEL_GUID, _treasureCorpMerchantAccountId, _treasureHuntCurrencyId, (int)Creel.CustomerWorkspace.CustomerWorkspaceEnumerations.SendOrderTargetTypes.Creel, string.Empty, model.TO_CREEL_GUID, model.Amount, model.Message, DateTime.UtcNow.AddDays(7), string.Empty);

                    var PFE = user.Balances.Where(t => t.CurrencyId == GameCurrenciesConstant.GOLD_ID).FirstOrDefault();
                    PFE.Amount -= model.Amount;

                    TZE.SaveChanges();

                }
                result.Status = ApiResponse.OK;
                result.Data = ConstantStringsMessages.CREEL_CODE_ACCEPTING_INSTRUCTION;
                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/UserSearch")]
        [HttpPost]
        public ApiResult UserSearch(UserSearchCreel modelUserSearch)
        {
            ApiResult result = new ApiResult();
            try
            {
                CustomerAccountManager customerAccountsManager = new CustomerAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, modelUserSearch.DeviceId);
                var customers = customerAccountsManager.GetCustomerAccountsStartingWith(modelUserSearch.CurrentCustomerAccountId, 3, modelUserSearch.SearchKeyword);
                result.Status = ApiResponse.OK;
                result.Data = customers;
                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }

        [Route("api/Creel/RegisterCreel")]
        [HttpPost]
        public ApiResult RegisterCreel(LogInModel model)
        {
   
                ApiResult result = new ApiResult();
                try
                {
                    using (Model1Container TZE = new Model1Container())
                    {
                        var UserLinq = TZE.Users.Where(t => (t.GUID == model.UserGuid)).FirstOrDefault();
                        if (UserLinq == null)
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = ConstantStringsMessages.INVALID_USER;
                            return result;
                        }

                        AuthorizationRequestManager authorizationRequestsManager = new AuthorizationRequestManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceId);
                        var creelServerResponse = authorizationRequestsManager.AddNewAuthorizationRequest(model.CreelUserName, model.DeviceName, model.DeviceId, model.Manufacturer, model.Model);

                        if (creelServerResponse == Guid.Empty)
                        {
                            throw new Exception(ConstantStringsMessages.CREEL_RETURN_EMPTY_GUID);
                        }

                        result.Status = ApiResponse.OK;
                        result.Data = creelServerResponse;
                        result.infoString = CREEL_WEBSITE;
                        return result;
                    }

                }
                catch (CreelException cE)
                {
                    result.Status = ApiResponse.CREEL_ERROR;
                    return creelErrorModifier(result, cE,model);
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME_CREEL, true, e);

                    result.Status = ApiResponse.CREEL_ERROR;
                    result.ErrorString = ConstantStringsMessages.CREEL_ERROR_MESSAGE;
                    return result;
                }
           
        }
        [Route("api/Creel/UnRegisterCreel")]
        [HttpPost]
        public ApiResult UnRegisterCreel(LogInModel model)
        {
            DeauthorizationRequestManager deauthorizeRequestManager = new DeauthorizationRequestManager(Guid.Parse(SERVER_ID),
              SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceId);

            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                try
                {
                    var UserLinq = TZE.Users.Where(t => (t.GUID == model.UserGuid)).FirstOrDefault();
                    if (UserLinq == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.INVALID_USER;
                        return result;
                    }
                    var deauthorizeResponse = deauthorizeRequestManager.AddNewDeauthorizationRequest(Guid.Parse(UserLinq.CreelGUID + ""),string.Empty);


                    if (deauthorizeResponse != null || deauthorizeResponse != Guid.Empty)
                    {
                        UserLinq.CreelGUID = Guid.Empty;
                        TZE.SaveChanges();
                    }

                    result.Status = ApiResponse.OK;
                    result.Data = ConstantStringsMessages.CREEL_UNLINK_FINISH; ;
                    return result;

                }
                catch (CreelException cE)
                {
                    return creelErrorModifier(result, cE);
                }
            }
        }

        [Route("api/Creel/CheckAuthorizationState")]
        [HttpPost]
        public ApiResult CheckAuthorizationState(LogInModel model)
        {
            ApiResult result = new ApiResult();
            try
            {
                if (model.CreelUserName == string.Empty)
                {
                    result.Status = ApiResponse.OK;
                    return result;
                }

                AuthorizationManager authorizationManager = new AuthorizationManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, model.DeviceId);
                var authorizationState = authorizationManager.GetAuthorizationState(model.CreelUserName);

                using (Model1Container TZE = new Model1Container())
                {
                    var UserLinq = TZE.Users.Where(t => (t.GUID == model.UserGuid)).FirstOrDefault();
                    if (UserLinq == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.INVALID_USER;
                        return result;
                    }
                    var creelAuthorizationResponse = IdentifyAuthorizationCode(authorizationState, model.CreelUserName, model.DeviceId, result);
                    if (creelAuthorizationResponse.Status == ApiResponse.OK)
                    {
                        UserLinq.CreelGUID = Guid.Parse(creelAuthorizationResponse.Data+"");
                        TZE.SaveChanges();
                        result.Status = ApiResponse.OK;
                        result.Data = authorizationState;
                    }
                    else
                    {
                        result = creelAuthorizationResponse;
                    }
                }

                return result;
            }
            catch (CreelException cE)
            {
                return creelErrorModifier(result, cE);
            }
        }
        public ApiResult creelErrorModifier(ApiResult result, CreelException cE, LogInModel loginModel = null)
        {
            ErrorSender errorSender = new ErrorSender();
            try
            {
                result.Status = ApiResponse.CREEL_ERROR;
                result.ErrorCode = cE.Code;

                switch (int.Parse(cE.Code))
                {
                    case (int)ConstantStringsMessages.CREEL_APPROVAL_CODE:
                        result.ErrorString = ConstantStringsMessages.CREEL_BUSINESS_RULES;
                        break;
                    case (int)ConstantStringsMessages.CREEL_APP_NOT_AUTHORIZE_TO_PERFORM_TASK_CODE:
                        result.ErrorString = ConstantStringsMessages.CREEL_APP_APPROVAL;
                        break;
                    case (int)ConstantStringsMessages.CREEL_INSUFFICIENT_FUNDS_CODE:
                        result.ErrorString = ConstantStringsMessages.CREEL_INVALID_FUND_BUY;
                        break;
                    case (int)ConstantStringsMessages.CREEL_VALIDATION_CODE:
                        result.ErrorString = ConstantStringsMessages.VALIDATION_ERROR;
                        break;
                    case (int)Creel.Exceptions.ErrorCodes.AuthorizationRequestExistingAlready:
                        // customer account id not yet available and we are not yet authorized to retrieve them
                        result.Status = ApiResponse.CREEL_ERROR;
                        result.ErrorString = ConstantStringsMessages.CREEL_EXISTING_REQUEST;
                        break;

                    case (int)Creel.Exceptions.ErrorCodes.AuthorizationGrantedAlready:
                        SaveCustomerCreelId(loginModel, out result);
                        break;
                    default:
                        result.ErrorString = ConstantStringsMessages.CREEL_ERROR_MESSAGE;
                        break;
                }
               
                errorSender.UploadError(ConstantStringsMessages.SERVER_NAME_CREEL, true, cE);
                return result;
            }
            catch (Exception e)
            {             
                errorSender.UploadError(ConstantStringsMessages.SERVER_NAME_CREEL, true, e);

                result.Status = ApiResponse.CREEL_ERROR;
                result.ErrorCode = cE.Code;
                result.ErrorString = ConstantStringsMessages.CREEL_APP_APPROVAL;
                return result;
            }
        }

        public ApiResult IdentifyAuthorizationCode(int authorizationCode, string creelUsername, string DeviceId, ApiResult result)
        {
            switch (authorizationCode)
            {
                case (int)Creel.CustomerWorkspace.CustomerWorkspaceEnumerations.AuthorizationStates.Authorized:
                case (int)Creel.CustomerWorkspace.CustomerWorkspaceEnumerations.AuthorizationStates.PendingDeauthorization:

                    // authorization granted already just get CreelUsername
                    CustomerAccountManager customerAccountManager = new CustomerAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, DeviceId);

                    var creelServerResponse = customerAccountManager.GetCustomerAccountIdByUsername(creelUsername);

                    if (creelServerResponse == Guid.Empty)
                    {
                        throw new Exception(ConstantStringsMessages.CREEL_RETURN_EMPTY_GUID);
                    }

                    result.Status = ApiResponse.OK;
                    result.Data = creelServerResponse;
                    result.infoString = CREEL_WEBSITE;

                    break;

                case (int)Creel.CustomerWorkspace.CustomerWorkspaceEnumerations.AuthorizationStates.PendingAuthorization:

                    // customer clicked continue but authorization request has not yet been approved
                    result.Status = ApiResponse.CREEL_PENDING;
                    result.ErrorString = ConstantStringsMessages.CREEL_PENDING_AUTHORIZATION;
                    result.infoString = CREEL_WEBSITE;

                    break;

                case (int)Creel.CustomerWorkspace.CustomerWorkspaceEnumerations.AuthorizationStates.Deauthorized:
                case (int)Creel.CustomerWorkspace.CustomerWorkspaceEnumerations.AuthorizationStates.Unauthorized:

                    result.Status = ApiResponse.CREEL_DEAUTHORIZE;
                    result.ErrorString = ConstantStringsMessages.CREEL_DENIED_AUTHORIZATION; ;
                    result.infoString = CREEL_WEBSITE;

                    break;
            }
            return result;
        }
        void SaveCustomerCreelId(LogInModel loginModel, out ApiResult result)
        {
            result = new ApiResult();
            using (Model1Container TZE = new Model1Container())
            {
                CustomerAccountManager customerAccountManager = new CustomerAccountManager(new Guid(SERVER_ID), SERVICE_CONSUMER_KEY, SERVICE_CONSUMER_SECRET, loginModel.DeviceId);
                var creelServerResponse = customerAccountManager.GetCustomerAccountIdByUsername(loginModel.CreelUserName);
                var UserLinq = TZE.Users.Where(t => (t.GUID == loginModel.UserGuid)).FirstOrDefault();
                result.Status = ApiResponse.OK;
                result.Data = creelServerResponse;
                UserLinq.CreelGUID = creelServerResponse;
                TZE.SaveChanges();
            }
        }
        public ApiResult changeIngameCurrency(TransferModel model, bool add)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    user = TZE.Users.Where(t => t.GUID == model.UserGuid).FirstOrDefault();

                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.INVALID_USER;
                    }

                    var PFE = user.Balances.Where(t => t.CurrencyId == GameCurrenciesConstant.GOLD_ID).FirstOrDefault();


                    if (add)
                        PFE.Amount += Convert.ToDouble(model.Amount);
                    else
                    {
                        if (PFE.Amount < Convert.ToDouble(model.Amount))
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = ConstantStringsMessages.CREEL_INVALID_FUND_TRANSFER;
                            return result;
                        }
                        PFE.Amount -= Convert.ToDouble(model.Amount);
                    }

                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;

                    return result;
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME_CREEL, true, e);

                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.CREEL_ERROR_MESSAGE;

                    return result;
                }
            }
        }
    }
}
