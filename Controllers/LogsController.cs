﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreasureGameModels;
using TreasureGameWeb.DAL;

namespace TreasureGameWeb.Controllers
{
    public class LogsController : ApiController
    {
        [Route("api/Logs/GetTransactionLogs")]
        [HttpPost]
        public ApiResult GetTransactionLogs(TransactionLogsModel transcationLogs)
        {
            using (Model1Container context = new Model1Container())
            {
                TransactionLogsModel transcationLogModelLocal = new TransactionLogsModel();
                transcationLogModelLocal.logModels = new List<LogsModel>();
                LogsModel logsmodel = null;
                var user = context.Users.Where(x => x.GUID == transcationLogs.UserGuid).FirstOrDefault();

                if (user != null)
                {
                    if (user.Exchange_Log == null || user.Exchange_Log.Count == 0)
                    {
                        return new ApiResult()
                        {
                            Status = ApiResponse.OTHER_ERROR,
                            ErrorString = "No logs Yet"
                        };
                    }
                    foreach (var transactLogs in user.Exchange_Log)
                    {
                        logsmodel = new LogsModel();
                        logsmodel.Message = transactLogs.Message.ToString();
                        logsmodel.date = transactLogs.Date;

                        transcationLogModelLocal.logModels.Add(logsmodel);
                    }

                    return new ApiResult()
                    {
                        Data = transcationLogModelLocal,
                        Status = ApiResponse.OK
                    };
                }
                else
                {
                    return new ApiResult()
                    {
                        Status = ApiResponse.OTHER_ERROR,
                        ErrorString = "User Not Valid"
                    };
                }
                   
            }
        }
    }
}
