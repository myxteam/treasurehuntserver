﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TreasureGameModels;
using TreasureGameWeb.Class;
using TreasureGameWeb.DAL;
using TreasureGameWeb.Models.Internal;


namespace TreasureGameWeb.Controllers
{
    public class BuyController : ApiController
    {
        [Route("api/Buy/GetPackages")]
        [HttpGet]
        public ApiResult GetPackages()
        {

            //Sample Packages
            List<GamePackageModel> listPackage = new List<GamePackageModel>();
            listPackage.Add(new GamePackageModel()
            {
                Name = "Package 1",
                GameCurrency = GameCurrencies.GOLD,
                PackageValue = 20,
                PackagePrice = 10
            });
            listPackage.Add(new GamePackageModel()
            {
                Name = "Package 2",
                GameCurrency = GameCurrencies.GOLD,
                PackageValue = 60,
                PackagePrice = 30
            });
            listPackage.Add(new GamePackageModel()
            {
                Name = "Package 3",
                GameCurrency = GameCurrencies.GOLD,
                PackageValue = 100,
                PackagePrice = 50
            });

            return new ApiResult() { 
                Data = listPackage,
                Status = ApiResponse.OK
            };
        }

        [Route("api/Buy/BuyPackage")]
        [HttpGet]
        public ApiResult BuyPackage(LogInModel model, GamePackageModel package)
        {
            using (Model1Container context = new Model1Container())
            {
                var userID = context.Users.Where(x => x.GUID == model.UserGuid).FirstOrDefault().Id;
                var wallet = context.Balances.Include("User").Where(x => x.User.Id == userID).ToList();

                var currency = wallet.Where(x => x.CurrencyId == (int)package.GameCurrency).FirstOrDefault();
                currency.Amount = currency.Amount + package.PackageValue;

                Trading_Log_Specific tradeLog = new Trading_Log_Specific() {
                    Sold_Amount = package.PackagePrice,
                    Sold_Currency = "Euro",
                    Bought_Amount = package.PackageValue,
                    Bought_Currency = package.GameCurrency.ToString()
                };

                Exchange_Log logUpdate = new Exchange_Log();
                logUpdate.Date = DateTime.UtcNow;
                logUpdate.Message = @"Bought " + tradeLog.Bought_Amount + " " + tradeLog.Bought_Currency +
                                     " for €" + tradeLog.Sold_Amount;
                logUpdate.TradingLogsSpecific = tradeLog;

                context.Trading_Log_Specific.Add(tradeLog);
                context.Exchange_Log.Add(logUpdate);
                context.SaveChanges();

                return new ApiResult()
                {
                    Data = "Buying package successful.",
                    Status = ApiResponse.OK
                };
            
            }
        }



        [Route("api/Buy/Peek")]
        [HttpPost]
        public ApiResult Peek(LogInModel model)
        {
            int charge = 200;
            int OGcharge = 200;
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    if (model.Password != null)
                    {
                        user = TZE.Users.Where(t => t.Username == model.UserName && t.Password == model.Password).FirstOrDefault();
                    }
                    else
                    {
                        user = TZE.Users.Where(t => t.GUID == model.UserGuid).FirstOrDefault();
                    }
                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid user";
                        return result;
                    }

                    string x_CurrencyName = "";
                    double TO_PAY = 0;
                    double TO_PAY_EUR = 0;
                    if (model.GameID.ToString() == ConstanStrings.THGameId)
                    {
                        // gold;
                        //  TO_PAY = TZE.Currencies.Where(t => t.Id == 1).FirstOrDefault().Sell * charge;
                        TO_PAY_EUR = TZE.Currencies.OfType<Exchange_Rate>().Where(t => t.Id == 1).FirstOrDefault().SellPrice;
                        TO_PAY = TO_PAY_EUR * charge;
                        x_CurrencyName = "Gold";
                    }
                    else
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid GameID";
                        return result;
                    }

                    var availCurr = user.Balances;
                    var existingCurr = TZE.Currencies.OfType<Exchange_Rate>().Where(t => t.Active == true);

                    WalletModel wmodel = new WalletModel();
                    wmodel.Currencies = new List<WalletModelEntry>();
                    wmodel.CurrencyName = x_CurrencyName;
                    wmodel.ChargeAmmount = OGcharge;

                    foreach (var eCurr in existingCurr)
                    {
                        if (TO_PAY > 0)
                        {
                            if (eCurr.Name != x_CurrencyName)
                            {
                                var isAvail = availCurr.Where(t => t.Id == eCurr.Id).FirstOrDefault();
                                if (isAvail != null)
                                {
                                    double e_Ammount = isAvail.Amount;
                                    if (e_Ammount > 0)
                                    {
                                        double WORTH = eCurr.SellPrice * e_Ammount;

                                        if (WORTH >= TO_PAY)
                                        {
                                            int chargingNow = (int)Math.Round(TO_PAY / eCurr.SellPrice, MidpointRounding.AwayFromZero);
                                            // isAvail.Ammount -= chargingNow;
                                            TO_PAY = 0;

                                            WalletModelEntry wme = new WalletModelEntry();
                                            wme.CurrencyName = eCurr.Name;
                                            wme.Amount = chargingNow;
                                            wmodel.Currencies.Add(wme);

                                        }
                                        else
                                        {
                                            WalletModelEntry wme = new WalletModelEntry();
                                            wme.CurrencyName = eCurr.Name;
                                            wme.Amount = isAvail.Amount;
                                            wmodel.Currencies.Add(wme);
                                            //  isAvail.Ammount = 0;
                                            TO_PAY -= WORTH;

                                        }
                                    }

                                }
                            }
                        }
                    }

                    if (TO_PAY > 0)
                    {
                        WalletModelEntry wme = new WalletModelEntry();
                        wme = new WalletModelEntry();
                        wme.CurrencyName = "EUR";
                        wme.Amount = (int)Math.Round(TO_PAY / 100, MidpointRounding.AwayFromZero);
                        wmodel.Currencies.Add(wme);

                    }

                    wmodel.Guid = model.GameID;
                    result.Data = wmodel;
                    result.Status = ApiResponse.OK;
                    return result;
                }
                catch (Exception e)
                {
                    Debug_Log log = new Debug_Log();
                    log.Date = DateTime.UtcNow;
                    log.Message = e.Message + " " + e.StackTrace;
                    TZE.Debug_Log.Add(log);
                    TZE.SaveChanges();
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = e.Message;
                    return result;
                }
            }
        }

        [Route("api/Buy/Buy")]
        [HttpPost]
        public ApiResult Buy(WalletModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user;
                try
                {
                    if (model.Password != null)
                    {
                        user = TZE.Users.Where(t => t.Username == model.Username && t.Password == model.Password).FirstOrDefault();
                    }
                    else
                    {
                        user = TZE.Users.Where(t => t.GUID == model.Guid).FirstOrDefault();
                    }
                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid user";
                        return result;
                    }

                    var userPurse = user.Balances;

                    foreach (var k in model.Currencies)
                    {
                        if (k.CurrencyName != "EUR")
                        {
                            var curId = TZE.Currencies.Where(t => t.Name == k.CurrencyName).FirstOrDefault().Id;

                            var k2 = userPurse.Where(t => t.Id == curId).FirstOrDefault();
                            k2.Amount -= k.Amount;
                            if (k2.Amount < 0)
                            {
                                result.Status = ApiResponse.OTHER_ERROR;
                                result.ErrorString = "Insufficent Funds";
                                return result;
                            }
                        }

                    }

                    if (model.DIRECT != 1000)
                    {
                        userPurse.Where(t => t.CurrencyId == GameCurrenciesConstant.GOLD_ID).FirstOrDefault().Amount += model.ChargeAmmount;
                    }
                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;
                    return result;
                }
                catch (Exception e)
                {
                    Debug_Log log = new Debug_Log();
                    log.Date = DateTime.UtcNow;
                    log.Message = e.Message + " " + e.StackTrace;
                    TZE.Debug_Log.Add(log);
                    TZE.SaveChanges();
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = e.Message;
                    return result;
                }
            }
        }

        [Route("api/Buy/GetStatus")]
        [HttpPost]
        public ApiResult GetStatus(LogInModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    if (model.Password != null)
                    {
                        user = TZE.Users.Where(t => t.Username == model.UserName && t.Password == model.Password).FirstOrDefault();
                    }
                    else
                    {
                        user = TZE.Users.Where(t => t.GUID == model.UserGuid).FirstOrDefault();
                    }
                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid user";
                        return result;
                    }


                    var userPurse = user.Balances;

                    WalletModel wmodel = new WalletModel();
                    wmodel.CurrencyName = "Gold";
                    wmodel.Currencies = new List<WalletModelEntry>();

                    foreach (var k in userPurse)
                    {
                        WalletModelEntry wme = new WalletModelEntry();
                        var x = TZE.Currencies.Where(t => t.Id == k.Id && t.Active == true).FirstOrDefault();
                        if (x != null)
                        {
                            wme.CurrencyName = x.Name;
                            wme.Amount = k.Amount;
                            wmodel.Currencies.Add(wme);
                        }
                    }

                    result.Data = wmodel;

                    result.Status = ApiResponse.OK;
                    return result;
                }
                catch (Exception e)
                {
                    Debug_Log log = new Debug_Log();
                    log.Date = DateTime.UtcNow;
                    log.Message = e.Message + " " + e.StackTrace;
                    TZE.Debug_Log.Add(log);
                    //TZE.SaveChanges();
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = e.Message;
                    return result;
                }
            }
        }

        [Route("api/Buy/TransferToWallet")]
        [HttpPost]
        public ApiResult TransferToWallet(TransferToWalletModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    if (model.LoginModel.Password != null)
                    {
                        user = TZE.Users.Where(t => t.Username == model.LoginModel.UserName && t.Password == model.LoginModel.Password).FirstOrDefault();
                    }
                    else
                    {
                        user = TZE.Users.Where(t => t.GUID == model.LoginModel.UserGuid).FirstOrDefault();
                    }

                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid user";
                        return result;
                    }

                    string x_CurrencyName = "";
                    if (model.GameID.ToString() == "57bca793-2632-4f0e-a7a8-031b3b83f6dd")
                    {
                        // gold;
                        x_CurrencyName = "Gold";
                    }
                    else if (model.GameID.ToString() == "081bd80a-a53e-4002-8793-c89957db71b8")
                    {
                        // silver
                        x_CurrencyName = "Silver";
                    }
                    else
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid GameID";
                        return result;
                    }

                    if (x_CurrencyName == "Gold")
                    {
                        var gold = user.Balances.Where(t => t.Id == 1).FirstOrDefault();
                        gold.Amount += model.Ammount;
                    }
                    else
                    {
                        var gold = user.Balances.Where(t => t.Id == 2).FirstOrDefault();
                        gold.Amount += model.Ammount;
                    }

                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;
                    return result;
                }
                catch (Exception e)
                {
                    Debug_Log log = new Debug_Log();
                    log.Date = DateTime.UtcNow;
                    log.Message = e.Message + " " + e.StackTrace;
                    TZE.Debug_Log.Add(log);
                    //TZE.SaveChanges();
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = e.Message;
                    return result;
                }
            }
        }

        [Route("api/Buy/TransferToGame")]
        [HttpPost]
        public ApiResult TransferToGame(TransferToGameModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    if (model.LoginModel.Password != null)
                    {
                        user = TZE.Users.Where(t => t.Username == model.LoginModel.UserName && t.Password == model.LoginModel.Password).FirstOrDefault();
                    }
                    else
                    {
                        user = TZE.Users.Where(t => t.GUID == model.LoginModel.UserGuid).FirstOrDefault();
                    }
                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid user";
                        return result;
                    }

                    string x_CurrencyName = "";
                    if (model.GameID.ToString() == ConstanStrings.THGameId)
                    {
                        // gold;
                        x_CurrencyName = GameCurrenciesConstant.GOLD;
                    }
                    else if (model.GameID.ToString() == ConstanStrings.ETHGameId)
                    {
                        // silver
                        x_CurrencyName = "Silver";
                    }
                    else
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid GameID";
                        return result;
                    }

                    if (x_CurrencyName == GameCurrenciesConstant.GOLD)
                    {
                        var gold = user.Balances.Where(t => t.Id == 1).FirstOrDefault();
                        if (gold.Amount < model.Ammount)
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = "Insufficent Funds !";
                            return result;
                        }
                        gold.Amount -= model.Ammount;
                    }
                    else
                    {
                        var gold = user.Balances.Where(t => t.Id == 2).FirstOrDefault();
                        if (gold.Amount < model.Ammount)
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = "Insufficent Funds !";
                            return result;
                        }
                        gold.Amount -= model.Ammount;
                    }


                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;
                    return result;
                }
                catch (Exception e)
                {
                    Debug_Log log = new Debug_Log();
                    log.Date = DateTime.UtcNow;
                    log.Message = e.Message + " " + e.StackTrace;
                    TZE.Debug_Log.Add(log);
                    //TZE.SaveChanges();
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = e.Message;
                    return result;
                }
            }
        }

        [Route("api/Buy/TransferToUser")]
        [HttpPost]
        public ApiResult TransferToUser(TransferToUserModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user_FROM;
                User user_TO;
                try
                {
                    user_FROM = TZE.Users.Where(t => t.Username == model.FROM_USERNAME).FirstOrDefault();

                    if (user_FROM == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid user";
                        return result;
                    }

                    user_TO = TZE.Users.Where(t => t.Username == model.TO_USERNAME).FirstOrDefault();

                    if (user_TO == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "No such user: " + model.TO_USERNAME;
                        return result;
                    }
                    string x_CurrencyName = "";
                    if (model.GameID.ToString() == ConstanStrings.THGameId)
                    {
                        // gold;
                        x_CurrencyName = GameCurrenciesConstant.GOLD;
                    }
                    else if (model.GameID.ToString() == ConstanStrings.ETHGameId)
                    {
                        // silver
                        x_CurrencyName = "Silver";
                    }
                    else
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = "Invalid GameID";
                        return result;
                    }
                    if (x_CurrencyName == GameCurrenciesConstant.GOLD)
                    {
                        var gold = user_FROM.Balances.Where(t => t.Id == 1).FirstOrDefault();
                        if (gold.Amount < model.Amount)
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = "Insufficent Funds !";
                            return result;
                        }
                        var gold2 = user_TO.Balances.Where(t => t.Id == 1).FirstOrDefault();
                        gold.Amount -= model.Amount;
                        gold2.Amount += model.Amount;
                    }
                    else
                    {
                        var gold = user_FROM.Balances.Where(t => t.Id == 2).FirstOrDefault();
                        if (gold.Amount < model.Amount)
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = "Insufficent Funds !";
                            return result;
                        }
                        var gold2 = user_TO.Balances.Where(t => t.Id == 2).FirstOrDefault();
                        gold.Amount -= model.Amount;
                        gold2.Amount += model.Amount;
                    }


                    //PushNotificationSender pns = new PushNotificationSender();
                    //if (x_CurrencyName == GameCurrenciesConstant.GOLD)
                    //{
                    //    pns.sendPush(user_FROM.Username, "You just sent User " + model.Amount + " Gold to User " + user_TO.Username);
                    //    pns.sendPush(user_TO.Username, "User " + user_FROM.Username + " just sent you " + model.Amount + " Gold to your Creel Account");
                    //}
                    //else
                    //{
                    //    pns.sendPush(user_FROM.Username, "You just sent User " + model.Amount + " Silver to User " + user_TO.Username);
                    //    pns.sendPush(user_TO.Username, "User " + user_FROM.Username + " just sent you " + model.Amount + " Silver to your Creel Account");
                    //}



                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;
                    return result;
                }
                catch (Exception e)
                {
                    //Debug_Log log = new Debug_Log();
                    //log.Date = DateTime.UtcNow;
                    //log.Message = e.Message + " " + e.StackTrace;
                    //TZE.Debug_Log.Add(log);
                    //TZE.SaveChanges();
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = e.Message;
                    return result;
                }
            }
        }
    }

}
