﻿using DominionErrorLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using TreasureGameModels;
using TreasureGameWeb.Class;
using TreasureGameWeb.DAL;


namespace TreasureGameWeb.Controllers
{
    public class LoginController : ApiController
    {
    
        [Route("api/Login/Login")]
        [HttpPost]
        public ApiResult Login(LogInModel model)
        {
            Debug.WriteLine("Login return ");
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                try
                {
                    using (MD5 md5Hash = MD5.Create())
                    {
                        string hash = GetMd5Hash(md5Hash, model.Password);
                        var UserLinq = TZE.Users.Where(t => ((t.Username == model.UserName && t.Password == hash) || t.GUID == model.UserGuid));
                        if (UserLinq.Count() == 0)
                        {
                            result.Status = ApiResponse.OTHER_ERROR;
                            result.ErrorString = ConstantStringsMessages.INVALID_USERNAME;
                            return result;
                        }

                        result.Status = ApiResponse.OK;
                        result.Data = UserLinq.FirstOrDefault().GUID;                   
                        return result;
                    }
                }
                catch (Exception e)
                {                    
                    try
                    {
                        ErrorSender errorSender = new ErrorSender();
                        errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, e);
                    }
                    catch (Exception ec)
                    {
                        ErrorSender errorSender = new ErrorSender();
                        errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, ec);
                    }

                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                    return result;
                }
            }
        }

        [Route("api/Login/Authenticate")]
        [HttpPost]
        public ApiResult Authenticate(LogInModel model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                try
                {
                    var UserLinq = TZE.Users.Where(t => (t.GUID == model.UserGuid));
                    if (UserLinq.Count() == 0)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.INVALID_USERNAME;
                        return result;
                    }

                    result.Status = ApiResponse.OK;
                    result.Data = UserLinq.FirstOrDefault().CreelGUID;
                    return result;
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, e);

                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;
                    return result;
                }
            }
        }
        string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash. 
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes 
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string. 
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string. 
            return sBuilder.ToString();
        }
    }
}
