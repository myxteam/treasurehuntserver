﻿using DominionErrorLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TimeZappWeb;
using TreasureGameModels;
using TreasureGameWeb.Class;
using TreasureGameWeb.DAL;


namespace TreasureGameWeb.Controllers
{
    public class GameEventController : ApiController
    {
        [Route("api/GameEvent/PostEvent")]
        [HttpPost]
        public ApiResult PostEvent(TreasureGameModels.GameEvent model)
        {
            using (Model1Container TZE = new Model1Container())
            {
                ApiResult result = new ApiResult();
                User user = null;
                try
                {
                    user = TZE.Users.Where(t => t.GUID == model.UserGuid).FirstOrDefault();

                    if (user == null)
                    {
                        result.Status = ApiResponse.OTHER_ERROR;
                        result.ErrorString = ConstantStringsMessages.INVALID_USER;
                        return result;
                    }

                    var PFE = user.Balances.Where(t => t.CurrencyId == GameCurrenciesConstant.GOLD_ID).FirstOrDefault();
                    PFE.Amount += model.Amount;
                    PFE.Amount -= model.Cost;
                    PFE.Amount = Math.Abs(PFE.Amount);

                    TZE.SaveChanges();
                    result.Status = ApiResponse.OK;

                    return result;
                }
                catch (Exception e)
                {
                    ErrorSender errorSender = new ErrorSender();
                    errorSender.UploadError(ConstantStringsMessages.SERVER_NAME, true, e);
                    result.Status = ApiResponse.OTHER_ERROR;
                    result.ErrorString = ConstantStringsMessages.ERROR_OCCURED;

                    return result;
                }
            }
        }
    }
}